$(function(){
	"use strict";
	ImgChange();
	
	// TOGGLE NAV	
	$( "#global-nav-area" ).hide();
	var flg = 'default';
	$( "#toggle" ).click( function(){

		$( "#toggle-btn-icon" ).toggleClass( "close" );
		if($( "#toggle-btn-icon" ).hasClass( "close" )){
			$("#toggle-btn p").text("CLOSE");
		} else {
			$("#toggle-btn p").text("MENU");
		}
		if( flg === "default" ){
			
			flg = "close";

			$( "#global-nav-area" ).addClass( "show" );

		}else{
			
			flg = "default";
			$( "#global-nav-area" ).removeClass( "show" );
			//$('.global-list li a').off();
		}

		$("#global-nav-area").slideToggle('slow');
	});
	
	$( "#global-nav-area a" ).click( function(){

		$( "#toggle-btn-icon" ).toggleClass( "close" );
		if($( "#toggle-btn-icon" ).hasClass( "close" )){
			$("#toggle-btn p").text("CLOSE");
		} else {
			$("#toggle-btn p").text("MENU");
		}
		if( flg === "default" ){
			
			flg = "close";

			$( "#global-nav-area" ).addClass( "show" );

		}else{
			
			flg = "default";
			$( "#global-nav-area" ).removeClass( "show" );
			//$('.global-list li a').off();
		}

		$("#global-nav-area").slideToggle('slow');
	});
	
	var topBtn = $('#page-top a');    
	topBtn.hide();
	
	//スクロールが500に達したらボタン表示
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
	});
	
	//スクロールしてトップ
	topBtn.click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 1500);
		return false;
	});
	
	$('#global-nav-area a').click(function(){	
		//ヘッダーがfixedによるアンカー調整
		var widimage = window.innerWidth;
		var value = 0;
		setTimeout(function(){			
			var speed2 = 10;
			var urlHash = location.hash;
			var target2 = $(urlHash);
			var value = 0;
			//別ページからの遷移時
			if (urlHash) {
				value = $('header').height();
				var position2 = target2.offset().top - value;
				$("html, body").animate({scrollTop:position2}, speed2, "swing");
				return false;
			}
		},10);
	});
	
	$(window).on('load', function(){
		var widimage = window.innerWidth;
		var value = 0;
		setTimeout(function(){			
			var speed2 = 10;
			var urlHash = location.hash;
			var target2 = $(urlHash);
			var value = 0;
			//別ページからの遷移時
			if (urlHash) {
				if( widimage < 1001 ){
					value = $('header').height();
				} else {
					value = 0;
				}
				var position2 = target2.offset().top - value;
				$("html, body").animate({scrollTop:position2}, speed2, "swing");
				return false;
			}
		},10);
		if( widimage < 1001 ){
			//$(".scrollBox01").mCustomScrollbar();
		} else {
			$(".scrollBox01").mCustomScrollbar();
		}
	});	
	
	
		
	$('a[href^=#]').click(function(){		
		var speed = 1000;
		var href= $(this).attr("href");
		var target = $(href === "#" || href === "" ? 'html' : href);
		var position = 0;
		
		if($(this).hasClass('is-pagescroll')){
			var widimage = window.innerWidth;
			if( widimage < 1001 ){
				position = target.offset().top - $('.l-header').height();
			} else {
				position = target.offset().top;
			}							
			$("html, body").animate({scrollTop:position}, speed, "swing");
			return false;
		} else if($(this).hasClass('is-concept')){
		
		} else {
			position = target.offset().top;			
			$("html, body").animate({scrollTop:position}, speed, "swing");
			return false;
		}
	});
	
	$(window).on('load resize', function(){
		ImgChange();
		setTimeout(function(){
			SPMenuHeight();
		},100);
					
	});
	
	if(navigator.userAgent.indexOf('Android') > 0){
        
    }
	var widimage = window.innerWidth;
	if( widimage < 1001 ){
		
	} else {
		$(".scrollBox01").mCustomScrollbar({
			theme:"dark"
		});
	}
	
});


function  ImgChange(){
	"use strict";
	// スマホ画像切替
	var widimage = window.innerWidth;
	//var widimage = parseInt($(window).width()) + 16;
	
	if( widimage < 1001 ){
		$('.is-imgChange').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
		
		$('.is-imgChange').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}
	
	if( widimage < 769 ){
		$('.is-imgChange2').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
	
		$('.is-imgChange2').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}
	
	if( widimage < 481 ){
		$('.is-imgChange3').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
	
		$('.is-imgChange3').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}
	
	/*2段階の変更*/
	if( widimage < 1001 ){
		if( widimage < 481 ){
			
			$('.is-imgChange_1').each( function(){
				$(this).attr("src",$(this).attr("src").replace('_pc.png', '_sp2.png'));
				$(this).attr("src",$(this).attr("src").replace('_sp.png', '_sp2.png'));
			});
		} else {
			$('.is-imgChange_1').each( function(){
				$(this).attr("src",$(this).attr("src").replace('_sp2.png', '_sp.png'));
				$(this).attr("src",$(this).attr("src").replace('_pc.png', '_sp.png'));
			});
		}
	} else {
		
		$('.is-imgChange_1').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp.png', '_pc.png'));
			$(this).attr("src",$(this).attr("src").replace('_sp2.png', '_pc.png'));
		});
	}
}

function  SPMenuHeight(){
	"use strict";
	var header_Height = $('header').height();
	var menu_Height = window.innerHeight - 70;
	//var menu_Height = window.innerHeight - 150;
	$('#global-nav-area').css('top',header_Height + 'px');
	$('#global-nav-area').css('height',menu_Height + 'px');
	$('#global-nav-area').css('overflow-y','auto');
	$('#global-nav-area').css('-webkit-overflow-scrolling','touch');
}



