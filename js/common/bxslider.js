
	
jQuery(document).ready(function($){
	var slider = $('.bxslider').bxSlider({
		mode: 'fade',
		auto: true,
		pause: 5000,
		speed: 3000,
		responsive: true,
		onSlideAfter: function(){
            slider.startAuto();
        }
	});	
});
