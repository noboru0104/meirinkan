$(function(){
	"use strict";
	
	TsuikaMethod();
	ContactValidate();
	
	$(window).load(function () {		
				
	});
	
	$(window).resize(function () {
		
	});
	
});

function  TsuikaMethod(){
	"use strict";
	
	//全角のみを追加
	jQuery.validator.addMethod("zenkaku", function(value, element) {
		return this.optional(element) || /^[^ -~｡-ﾟ]*$/.test(value);
		}, "全角文字で入力してください。"
	);
	
	//全角カタカナのみ
	jQuery.validator.addMethod("zenkatakana", function(value, element) {
		return this.optional(element) || /^([ァ-ヶー]+)$/.test(value);
		}, "全角カタカナで入力してください。"
	);
	
	//電話番号
	jQuery.validator.addMethod("tel2", function(value, element) {
		return this.optional(element) ||/^[0-9\-]+$/.test(value);
		}, "電話番号は半角数字と半角ハイフンで入力してください。例）080-1234-5678"
	);
	
	//jQuery.validator.addMethod("valueNotEquals", function(value, element, arg){  
//		return arg !== value;  
//		}, "selectの値が同じだったらエラーを返す"
//	); 
	
}

function  ContactValidate(){
	"use strict";
	
	$("#contact").validate({
		
		rules: {
			//保護者のお名前 姓-(必須)
			'contact_last_name': {
				required: true
			},
			//保護者のお名前 名-(必須)
			'contact_first_name': {
				required: true
			},
			//保護者のフリガナ セイ-(必須)
			'contact_last_name_hurigana': {
				required: true,
				zenkatakana: true
			},
			//保護者のフリガナ メイ-(必須)
			'contact_first_name_hurigana': {
				required: true,
				zenkatakana: true
			},
			//学年-(必須&半角数字)
			'contact_gakunen': {
				required: true,
				number: true
			},
			//メールアドレス-(必須&メール形式)
			'contact_mail': {
				required: true,
				email: true
			},
			//電話番号-(必須&半角数字)
			'contact_tel': {
				required: true,
				tel2: true
			},
			//お問い合わせ内容-(必須)
			'other': {
				required: true
			},
		},
		messages: {
			'contact_last_name': {
				required: '保護者のお名前は必須入力です。'
			},
			'contact_first_name': {
				required: '保護者のお名前は必須入力です。'
			},
			'contact_last_name_hurigana': {
				required: '保護者のフリガナは必須入力です。',
				zenkatakana: '保護者のフリガナは全角カタカナで入力してください。'
			},
			'contact_first_name_hurigana': {
				required: '保護者のフリガナは必須入力です。',
				zenkatakana: '保護者のフリガナは全角カタカナで入力してください。'
			},
			'contact_gakunen': {
				required: '学年は必須入力です。',
				number: '学年は半角数字で入力してください。'
			},
			'contact_mail': {
				required: 'この項目は必須入力です。',
				email: 'メールアドレスの形式が正しくありません。'
			},
			'contact_tel': {
				required: 'この項目は必須入力です。',
				tel2: '電話番号は半角数字とハイフンで入力してください。例）080-1234-5678'
			},
			'other': {
				required: 'お問い合わせ内容は必須入力です。'
			},
		},  
		groups: {  
			contact_name: "contact_last_name contact_first_name",
			contact_hurigana: "contact_last_name_hurigana contact_first_name_hurigana",
		},		
		
		errorPlacement: function(error,element){			
			
			error.insertAfter($('#'+ element.attr('name') + '_err'));
			error.insertAfter($('#'+ element.attr('name') + '_err')).addClass('error_message_block');

		}
		
	});
	
}



