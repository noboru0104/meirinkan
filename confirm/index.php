<?php
session_start();
header("Content-type: text/html; charset=UTF-8");
mb_language("Japanese");
mb_internal_encoding("UTF-8");
error_reporting(E_ALL ^ E_NOTICE);

if(isset($_POST['contact_last_name'],$_POST['contact_first_name'],$_POST['contact_last_name_hurigana'],$_POST['contact_first_name_hurigana'],$_POST['contact_gakunen'],$_POST['contact_mail'],$_POST['contact_tel'],$_POST['other'])){
	$_SESSION['contact_last_name'] = htmlentities($_POST['contact_last_name'], ENT_QUOTES, "UTF-8");
	$_SESSION['contact_first_name'] = htmlentities($_POST['contact_first_name'], ENT_QUOTES, "UTF-8");
	$_SESSION['contact_last_name_hurigana'] = htmlentities($_POST['contact_last_name_hurigana'], ENT_QUOTES, "UTF-8");
	$_SESSION['contact_first_name_hurigana'] = htmlentities($_POST['contact_first_name_hurigana'], ENT_QUOTES, "UTF-8");
	$_SESSION['contact_gakunen'] = htmlentities($_POST['contact_gakunen'], ENT_QUOTES, "UTF-8");
	$_SESSION['contact_email'] = htmlentities($_POST['contact_mail'], ENT_QUOTES, "UTF-8");
	$_SESSION['contact_tel'] = htmlentities($_POST['contact_tel'], ENT_QUOTES, "UTF-8");
	$_SESSION['other'] = htmlentities($_POST['other'], ENT_QUOTES, "UTF-8");
	
	$_SESSION['contact_name'] = $_SESSION['contact_last_name'] ."　" .$_SESSION['contact_first_name'];
	$_SESSION['contact_name_hurigana'] = $_SESSION['contact_last_name_hurigana'] ."　" .$_SESSION['contact_first_name_hurigana'];
	$_SESSION['contact_gakunen'] = $_SESSION['contact_gakunen'] ."年生";
}
?>
<!doctype html>
<html>
<head>
<meta name="robots" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="keywords" content="中学受験 明倫館,高島平,学習塾,明倫館,板橋区">
<meta name="description" content="楽しむ子は伸びる。中学受験　明倫館。明倫館はご家族と一緒に中学入試を楽しむ塾です。">
<meta name="format-detection" content="telephone=no">
<title>お問い合わせ確認｜中学受験明倫館</title>
<!-- ▼共通CSS▼ -->
<link rel="stylesheet" type="text/css" href="../css/common/reset.css">
<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Noto+Serif+JP" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/common/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" type="text/css" href="../css/common/common.css">
<link rel="stylesheet" type="text/css" href="../css/contact/contact.css">
<!-- ▲共通CSS▲ -->
<!--<link rel="shortcut icon" href="images/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="images/favicon.ico" type="image/vnd.microsoft.icon">-->
<!-- ▼個別CSS▼ -->
<!--<link rel="stylesheet" href="../css/common/animate.css">-->
<!-- ▲個別CSS▲ -->
<!-- ▼共通JS▼ -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/common/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="../js/common/script.js"></script>
<!-- ▲共通JS▲ -->
<!-- ▼個別JS▼ -->
<!--<script type="text/javascript" src="../js/common/wow.js"></script>
<script type="text/javascript" src="../js/common/wow.min.js"></script>-->
<script type="text/javascript" src="../js/contact/contact.js"></script>
        <script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/common/jquery.matchHeight.js"></script>
<!-- ▲個別JS▲ -->
</head>
<body class="confirm">
<header>   
    <div class="l-header scrollBox01">
        <div class="l-headerInner">
            <h1 class="p-logo"><a href="../"><img src="../images/common/img_header_logo_pc.png" alt="楽しむ子は伸びる。中学受験明倫館"></a></h1>
            <div class="l-menu">
                <p><a class="is-pagescroll" href="../#01">明倫館の5つの特徴</a></p>
                <p><a class="is-pagescroll" href="../#02">明倫館のここが良い！</a></p>
                <p><a class="is-pagescroll" href="../#03">明倫館に決めた理由</a></p>
                <p><a class="is-pagescroll" href="../#04">合格者の声</a></p>
                <p><a class="is-pagescroll" href="../#05">塾長のごあいさつ</a></p>
                <p><a class="is-pagescroll" href="../#06">合格実績</a></p>
                <p><a class="is-pagescroll" href="../#07">Q&amp;A<!--よくある質問--></a></p>
                <p><a class="is-pagescroll" href="../#08">塾の概要</a></p>
            </div>
            <p class="p-button"><a href="../form/form.html"><img src="../images/common/btn_header_09_pc.png" alt="無料体験授業 随時受付中 ご予約はこちらから"></a></p>
            <p class="p-button"><a href="tel:0353995979"><img src="../images/common/btn_header_10_pc.png" alt="お電話でのお問い合わせはこちら 03-5399-5979（14時～19時 日曜は除く）"></a></p>
            <p class="p-button"><a href="../form/form.html"><img src="../images/common/btn_header_11_pc.png" alt="メールでのお問い合わせはこちら"></a></p>
        	
            <div class="l-header-right sponly0">
            	<!--SPハンバーガーメニュー-->
                <div id="toggle_block" class="sponly1200">                	
                    <div id="toggle">
                        <div id="toggle-btn">
                            <span id="toggle-btn-icon"></span><p class="Mincho">Menu</p>
                        </div>
                    </div><!--/#toggle-->
                </div><!--/#toggle_block-->
            </div>
        </div>
        <nav>
            <div id="global-nav-area">
                <ul id="global-nav">
                    <li><p><a class="is-pagescroll" href="../#01">明倫館の5つの特徴</a></p></li>
                    <li><p><a class="is-pagescroll" href="../#02">明倫館のここが良い！</a></p></li>
                    <li><p><a class="is-pagescroll" href="../#03">明倫館に決めた理由</a></p></li>
                    <li><p><a class="is-pagescroll" href="../#04">合格者の声</a></p></li>
                    <li><p><a class="is-pagescroll" href="../#05">塾長のごあいさつ</a></p></li>
                    <li><p><a class="is-pagescroll" href="../#06">合格実績</a></p></li>
                    <li><p><a class="is-pagescroll" href="../#07">Q&amp;A</a></p></li>
                    <li class="p-last"><p><a class="is-pagescroll" href="../#08">塾の概要</a></p></li>
                </ul>
            </div>
        </nav>
    </div>
</header>
    
<div id="wrapper">
	<div class="l-wrapper">
    	<section>
            <div class="l-inner754">
                <div class="l-contactBlock01">
                    <div class="l-contactBlock01-01">
                        <p class="p-title Mincho">お問い合わせ確認</p>
                        <p class="p-caution">入力内容をご確認の上、「送信する」ボタンをクリックしてください。</p>
                    </div>
                </div>
                <div class="l-contactBlock02">
                    
                    <form method="post" action="../complete/" id="contact">
                    <table>
                    	<tr>
                        	<th>保護者のお名前</th>
                            <td>
                            	<p>
                                	<?php echo $_SESSION['contact_name'] ?>
                                </p>
                            </td>
                        </tr>
                    	<tr>
                        	<th>保護者のフリガナ</th>
                            <td>
                            	<p>
                                	<?php echo $_SESSION['contact_name_hurigana'] ?>
                                </p>
                            </td>
                        </tr>
                    	<tr>
                        	<th>学年</th>
                            <td>
                            	<?php echo $_SESSION['contact_gakunen'] ?>
                            </td>
                        </tr>
                    	<tr>
                        	<th>メールアドレス</th>
                            <td>
                            	<?php echo $_SESSION['contact_email'] ?>
                            </td>
                        </tr>
                    	<tr>
                        	<th>電話番号</th>
                            <td>
                            	<?php echo $_SESSION['contact_tel'] ?>
                            </td>
                        </tr>
                    	<tr>
                        	<th class="p-toiawase">
                            	お問い合わせ内容
                            </th>
                            <td>
                            	<?php echo nl2br($_SESSION['other']) ?>
                            </td>
                        </tr>
                    </table>
                    <p class="p-submit">
                        <a class="p-back" href="javascript:history.back();">戻る</a><input type="submit" value="送信する">
                    </p>
                    </form>
                </div>
            </div>
    	</section>
        
    </div>
    
    <section>
        <div id="page-top">
            <div><a href="#"><img src="../images/common/btn_page_top_pc.png" alt="TOP"></a></div>
        </div>
	</section>

    <footer>
        <p><a href="../privacy/">プライバシーポリシー</a></p>
        <p>&copy;chugakujyuken meirinkan All Rights Reserved.</p>
    </footer>
    
</div>

</body>
</html>
