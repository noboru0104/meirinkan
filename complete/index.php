<?php
session_start();
header("Content-type: text/html; charset=UTF-8");
mb_language("Japanese");
mb_internal_encoding("UTF-8");
//mb_detect_order("ASCII, JIS, UTF-8, EUC-JP, SJIS");
error_reporting(E_ALL ^ E_NOTICE);

$to_name = $_SESSION['contact_name']." 様";
$to_addr = $_SESSION['contact_email'];
$to_name_enc = mb_encode_mimeheader($to_name, "ISO-2022-JP");
$to = "$to_name_enc <$to_addr>";
$re_from = $to;

$adress = "meirinkan@r4.dion.ne.jp";//本アドレス

$from_name = "明倫館";
$from_addr = $adress;//会社側
$reply_name = "明倫館";
$reply_addr = $adress;//会社側

$from_name_enc = mb_encode_mimeheader($from_name, "ISO-2022-JP");
$from = "$from_name_enc <$from_addr>";
$re_to = $from;

$reply_name_enc = mb_encode_mimeheader($reply_name, "ISO-2022-JP");
$reply = "$reply_name_enc <$reply_addr>";

$header  = "MIME-Version: 1.0\r\n";
$header .= "Content-Transfer-Encoding: 7bit\r\n";
$header .= "Content-Type : text/plain;\r\n";
$header .= "\tcharset=\"iso-2022-jp\";\r\n";
$re_header = $header;
$header .= "From: $from\r\n";
$header .= "Reply-To: $reply\r\n";

$subject =  "【中学受験明倫館】お問い合わせありがとうございます。（自動返信）";

//メールメッセージ文面（入力者用）
$body_text = <<< EOF

この度は、中学受験明倫館にお問い合わせいただきまして、誠にありがとうございます。
以下の内容でお問い合わせが完了いたしました。
※このメールは自動返信メールです。

EOF;

$body .= "\n";
$body .= "-----"."\n";
$body .= "保護者のお名前：" .$_SESSION['contact_name'] ."\n";
$body .= "保護者のフリガナ：" .$_SESSION['contact_name_hurigana'] ."\n";
$body .= "学年：" .$_SESSION['contact_gakunen'] ."\n";
$body .= "メールアドレス：" .$_SESSION['contact_email'] ."\n";
$body .= "電話番号：" .$_SESSION['contact_tel'] ."\n";
$body .= "お問い合わせ内容：" .strip_tags($_SESSION['other'], '<br />')."\n";
$body .= "-----"."\n";
$body .= "担当者より、あらためてご連絡いたします。よろしくお願いいたします。"."\n";
$body = $body_text.$body;
$body .= <<< EOF

===================================
中学受験明倫館
Email: meirinkan@r4.dion.ne.jp
URL: http://www.chugakujuken-meirinkan.net/
TEL: 03-5399-5979（14:00～19:00　日曜を除く）
===================================

EOF;
mb_send_mail($to, $subject, $body, $header,"-f$from_addr");



$re_header .= "From: $re_from\r\n";
$re_header .= "Reply-To: $re_from\r\n";
$re_subject = "ホームページからお問い合わせがありました。";
$re_to = $from;

//メールメッセージ文面（管理者用）
$re_body_text = <<< EOF

ホームページからお問い合わせがありました。

EOF;

$re_body .= "\n";
$re_body .= "-----"."\n";
$re_body .= "保護者のお名前：" .$_SESSION['contact_name'] ."\n";
$re_body .= "保護者のフリガナ：" .$_SESSION['contact_name_hurigana'] ."\n";
$re_body .= "学年：" .$_SESSION['contact_gakunen'] ."\n";
$re_body .= "メールアドレス：" .$_SESSION['contact_email'] ."\n";
$re_body .= "電話番号：" .$_SESSION['contact_tel'] ."\n";
$re_body .= "お問い合わせ内容：" .strip_tags($_SESSION['other'], '<br />')."\n";
$re_body .= "-----"."\n";
$re_body = $re_body_text.$re_body;

mb_send_mail($re_to, $re_subject, $re_body, $re_header,"-f$to_addr");

$_SESSION = array();
setcookie( session_name(), '', time()-60);
session_destroy();
?>
<!doctype html>
<html>
<head>
<meta name="robots" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="keywords" content="中学受験 明倫館,高島平,学習塾,明倫館,板橋区">
<meta name="description" content="楽しむ子は伸びる。中学受験　明倫館。明倫館はご家族と一緒に中学入試を楽しむ塾です。">
<meta name="format-detection" content="telephone=no">
<title>お問い合わせ完了｜中学受験明倫館</title>
<!-- ▼共通CSS▼ -->
<link rel="stylesheet" type="text/css" href="../css/common/reset.css">
<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Noto+Serif+JP" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/common/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" type="text/css" href="../css/common/common.css">
<link rel="stylesheet" type="text/css" href="../css/contact/contact.css">
<!-- ▲共通CSS▲ -->
<!--<link rel="shortcut icon" href="images/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="images/favicon.ico" type="image/vnd.microsoft.icon">-->
<!-- ▼個別CSS▼ -->
<!--<link rel="stylesheet" href="../css/common/animate.css">-->
<!-- ▲個別CSS▲ -->
<!-- ▼共通JS▼ -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/common/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="../js/common/script.js"></script>
<!-- ▲共通JS▲ -->
<!-- ▼個別JS▼ -->
<!--<script type="text/javascript" src="../js/common/wow.js"></script>
<script type="text/javascript" src="../js/common/wow.min.js"></script>-->
<script type="text/javascript" src="../js/contact/contact.js"></script>
        <script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="../js/common/jquery.matchHeight.js"></script>
<!-- ▲個別JS▲ -->
</head>
<body class="complete">
<header>    
    <div class="l-header scrollBox01">
        <div class="l-headerInner">
            <h1 class="p-logo"><a href="../"><img src="../images/common/img_header_logo_pc.png" alt="楽しむ子は伸びる。中学受験明倫館"></a></h1>
            <div class="l-menu">
                <p><a class="is-pagescroll" href="../#01">明倫館の5つの特徴</a></p>
                <p><a class="is-pagescroll" href="../#02">明倫館のここが良い！</a></p>
                <p><a class="is-pagescroll" href="../#03">明倫館に決めた理由</a></p>
                <p><a class="is-pagescroll" href="../#04">合格者の声</a></p>
                <p><a class="is-pagescroll" href="../#05">塾長のごあいさつ</a></p>
                <p><a class="is-pagescroll" href="../#06">合格実績</a></p>
                <p><a class="is-pagescroll" href="../#07">Q&amp;A<!--よくある質問--></a></p>
                <p><a class="is-pagescroll" href="../#08">塾の概要</a></p>
            </div>
            <p class="p-button"><a href="../form/form.html"><img src="../images/common/btn_header_09_pc.png" alt="無料体験授業 随時受付中 ご予約はこちらから"></a></p>
            <p class="p-button"><a href="tel:0353995979"><img src="../images/common/btn_header_10_pc.png" alt="お電話でのお問い合わせはこちら 03-5399-5979（14時～19時 日曜は除く）"></a></p>
            <p class="p-button"><a href="../form/form.html"><img src="../images/common/btn_header_11_pc.png" alt="メールでのお問い合わせはこちら"></a></p>
        	
            <div class="l-header-right sponly0">
            	<!--SPハンバーガーメニュー-->
                <div id="toggle_block" class="sponly1200">                	
                    <div id="toggle">
                        <div id="toggle-btn">
                            <span id="toggle-btn-icon"></span><p class="Mincho">Menu</p>
                        </div>
                    </div><!--/#toggle-->
                </div><!--/#toggle_block-->
            </div>
        </div>
        <nav>
            <div id="global-nav-area">
                <ul id="global-nav">
                    <li><p><a class="is-pagescroll" href="../#01">明倫館の5つの特徴</a></p></li>
                    <li><p><a class="is-pagescroll" href="../#02">明倫館のここが良い！</a></p></li>
                    <li><p><a class="is-pagescroll" href="../#03">明倫館に決めた理由</a></p></li>
                    <li><p><a class="is-pagescroll" href="../#04">合格者の声</a></p></li>
                    <li><p><a class="is-pagescroll" href="../#05">塾長のごあいさつ</a></p></li>
                    <li><p><a class="is-pagescroll" href="../#06">合格実績</a></p></li>
                    <li><p><a class="is-pagescroll" href="../#07">Q&amp;A</a></p></li>
                    <li class="p-last"><p><a class="is-pagescroll" href="../#08">塾の概要</a></p></li>
                </ul>
            </div>
        </nav>
    </div>
</header>
    
<div id="wrapper">
	<div class="l-wrapper">
    	<section>
            <div class="l-inner754">
                <div class="l-contactBlock01">
                	<div class="l-contactBlock01-01">
                        <p class="p-title Mincho">お問い合わせ完了</p>
                        <p class="p-caution">
                            お問い合わせ内容の送信が完了しました。<br><br>
                            原則として、3日以内にお返事をしております。<br>
                            （年末年始、お盆期間、ゴールデンウイーク等を除く）<br><br>
                            自動返信メールが届かない、3日以上経過しても返信がない場合は、<br>
                            お手数ですが電話にて直接ご連絡・お問い合わせくださいますようお願い申し上げます。
                        </p>
                    </div>
                </div>
                <div class="l-contactBlock02">                    
                    <p class="p-submit">
                        <a class="p-top" href="../">TOPへ</a>
                    </p>
                </div>
            </div>
    	</section>
        
    </div>
    
    <section>
        <div id="page-top">
            <div><a href="#"><img src="../images/common/btn_page_top_pc.png" alt="TOP"></a></div>
        </div>
	</section>

    <footer>
        <p><a href="../privacy/">プライバシーポリシー</a></p>
        <p>&copy;chugakujyuken meirinkan All Rights Reserved.</p>
    </footer>
    
</div>

</body>
</html>
